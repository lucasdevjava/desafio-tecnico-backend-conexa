package br.com.lucas.conexa.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.google.gson.Gson;

import br.com.lucas.conexa.repository.BlackListResponsitory;
import br.com.lucas.conexa.security.exception.JWTBlackList;
import br.com.lucas.conexa.service.UserDetailsServiceImpl;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

	private JWTUtil jwtUtil;

	private UserDetailsServiceImpl userDetailsService;

	private BlackListResponsitory blackListResponsitory;

	public JWTAuthorizationFilter(AuthenticationManager authenticationManager, JWTUtil jwtUtil,
			UserDetailsServiceImpl userDetailsService, BlackListResponsitory blackListResponsitory) {
		super(authenticationManager);
		this.jwtUtil = jwtUtil;
		this.userDetailsService = userDetailsService;
		this.blackListResponsitory = blackListResponsitory;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request,
			HttpServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		String header = request.getHeader("Authorization");

		var blackListedToken = blackListResponsitory.findByToken(header);
		if (blackListedToken == null) {

			if (header != null && header.startsWith("Bearer ")) {
				UsernamePasswordAuthenticationToken auth = getAuthentication(header.substring(7));
				if (auth != null) {
					SecurityContextHolder.getContext().setAuthentication(auth);
				}
			}
			chain.doFilter(request, response);
		}else{
			var gson = new Gson();
			response.setStatus(401);
			response.setContentType("application/json");
			response.getWriter().append(gson.toJson( new JWTBlackList()));
		}

	}

	private UsernamePasswordAuthenticationToken getAuthentication(String token) {
		if (jwtUtil.tokenValido(token)) {
			String username = jwtUtil.getUsername(token);
			UserDetails user = userDetailsService.loadUserByUsername(username);
			return new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
		}
		return null;
	}

}
