package br.com.lucas.conexa.security;

import java.util.Collection;
import java.util.UUID;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Data;
@Data
public class UserSS implements UserDetails {
	private static final long serialVersionUID = 1L;

	private UUID id;
	private String email;
	private String password;
	private Collection<? extends GrantedAuthority> authorities;

	
	public UserSS() {
		
	}


	public UserSS(UUID id, String email, String password) {
		super();
		this.id = id;
		this.email = email;
		this.password = password;
	
	}

 


	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}


	@Override
	public String getPassword() {
		return password;
	}


	@Override
	public String getUsername() {
		return email;
	}


	@Override
	public boolean isAccountNonExpired() {
		return true;
	}


	@Override
	public boolean isAccountNonLocked() {
		return true;
	}


	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}


	@Override
	public boolean isEnabled() {
		return true;
	}
	
 
}
    

