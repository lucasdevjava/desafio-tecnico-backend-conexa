package br.com.lucas.conexa.security.exception;

import java.util.Date;

import lombok.Data;
@Data
public class JWTBlackList {
    

    private Integer status = 401;
    private long timestamp = new Date().getTime();
    private String error = "Não autorizado";
    private String message = "Token na black list";
    private String path = "/not";


}
