package br.com.lucas.conexa.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import br.com.lucas.conexa.security.exception.*;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import br.com.lucas.conexa.dtos.CredicialDTO;
import br.com.lucas.conexa.dtos.TokenDTO;
 

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private AuthenticationManager authenticationManager;

 

	public JWTAuthenticationFilter(AuthenticationManager authenticationManager ) {
		 
		setAuthenticationFailureHandler(new JWTAuthenticationFailureHandler());
		this.authenticationManager = authenticationManager;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		try {

			CredicialDTO cre = new ObjectMapper().readValue(request.getInputStream(), CredicialDTO.class);

			return this.authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					cre.getEmail(), cre.getSenha(), new ArrayList<>()));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {

		String email = ((UserSS) authResult.getPrincipal()).getUsername();
		Collection<? extends GrantedAuthority> profile = ((UserSS) authResult.getPrincipal()).getAuthorities();
		UUID id = ((UserSS) authResult.getPrincipal()).getId();
		String token = generateToken(email, id, profile);
	    response.setStatus(200);
		response.setContentType("application/json");
		response.getWriter().append(token);

	}

	private class JWTAuthenticationFailureHandler implements AuthenticationFailureHandler {

		@Override
		public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
				AuthenticationException exception) throws IOException, ServletException {
		
		    var gson =new  Gson();
		    response.setStatus(401);
			response.setContentType("application/json");
			response.getWriter().append(gson.toJson(new JWTAuthenticationFailure()));
		}
 
	}

	public String generateToken(String email, UUID id, Collection<? extends GrantedAuthority> profile) {

		String token = Jwts.builder().setSubject(email)
				.claim("id", id.toString())
				.claim("email", email)
				.claim("profile", profile)
				.setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS512, SecurityConstants.SECRET).compact();

		Gson gson = new Gson();

		var tokemObejeto = new TokenDTO(token);

		return gson.toJson(tokemObejeto);

	}

}
