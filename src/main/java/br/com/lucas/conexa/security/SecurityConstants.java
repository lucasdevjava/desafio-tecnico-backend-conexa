package br.com.lucas.conexa.security;

public class SecurityConstants {
    

    public static final String SECRET = "ChoraPaixao";
	public static final String TOKEN_PREXI = "Bearer ";
	public static final String HEADER_STRING = "Authorization";
	public static final long EXPIRATION_TIME = 86400000L;

}
