package br.com.lucas.conexa.security.exception;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JWTAuthenticationFailure {

    private Integer status = 401;
    private long timestamp = new Date().getTime();
    private String error = "Não autorizado";
    private String message = "Email ou senha inválidos";
    private String path = "/login";

}

