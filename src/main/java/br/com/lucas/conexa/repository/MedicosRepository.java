package br.com.lucas.conexa.repository;

import javax.transaction.Transactional;

import br.com.lucas.conexa.models.Medicos;

@Transactional
public interface MedicosRepository extends PessoaBaseRepository<Medicos> {
    public Medicos findByEmail(String email);
}
