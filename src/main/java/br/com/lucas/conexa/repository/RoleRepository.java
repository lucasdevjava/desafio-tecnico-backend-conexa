package br.com.lucas.conexa.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.lucas.conexa.models.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, UUID>{
	Role findBynomeRole(String nomeRole);
	
}
