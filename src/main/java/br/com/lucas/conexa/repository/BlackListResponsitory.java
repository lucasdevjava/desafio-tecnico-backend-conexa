package br.com.lucas.conexa.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.lucas.conexa.models.*;

@Repository
public interface BlackListResponsitory extends JpaRepository<BlackList,UUID>   {
    
    
    BlackList findByToken(String token);
}