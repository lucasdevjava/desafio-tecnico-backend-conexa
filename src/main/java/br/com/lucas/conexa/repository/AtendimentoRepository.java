package br.com.lucas.conexa.repository;

import org.springframework.stereotype.Repository;

import br.com.lucas.conexa.models.Atendimento;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface AtendimentoRepository   extends JpaRepository<Atendimento,UUID>  {
    
}

