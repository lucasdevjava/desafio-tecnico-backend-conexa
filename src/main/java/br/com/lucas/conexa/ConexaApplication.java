package br.com.lucas.conexa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConexaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConexaApplication.class, args);
	
		System.out.println("\n\n || Dificuldades preparam pessoas comuns para destinos extraordinários. 'As Crônicas de Nárnia' ||");

	}

}
