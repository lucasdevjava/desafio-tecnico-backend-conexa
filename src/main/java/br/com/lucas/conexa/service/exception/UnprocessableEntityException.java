package br.com.lucas.conexa.service.exception;

public class UnprocessableEntityException extends RuntimeException {
	private static final long serialVersionUID = 1L;
    
    public UnprocessableEntityException(String msg){
        super(msg);
    }
}