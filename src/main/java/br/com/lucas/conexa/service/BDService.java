package br.com.lucas.conexa.service;

import static java.lang.System.out;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lucas.conexa.models.Role;
import br.com.lucas.conexa.models.enums.Perfil;
import br.com.lucas.conexa.repository.RoleRepository;

@Service
public class BDService {

    @Autowired
    private RoleRepository roleRepository;

    public void inicializarElementosNoBanco() {

        if (roleRepository.count() < 1) {

            for (Perfil perfil : Perfil.values()) {
                var role = new Role(perfil.name());
                roleRepository.save(role);
            }
          out.println("-Criação dos Perfil.-");
        }
    }

}
