package br.com.lucas.conexa.service;

import static br.com.lucas.conexa.security.SecurityConstants.HEADER_STRING;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lucas.conexa.security.JWTUtil;
import br.com.lucas.conexa.models.Atendimento;
import br.com.lucas.conexa.models.Medicos;
import br.com.lucas.conexa.repository.AtendimentoRepository;
import br.com.lucas.conexa.repository.MedicosRepository;

@Service
public class AtendimentoService {

    @Autowired
    private AtendimentoRepository atendimentoRepository;
    @Autowired
    private JWTUtil jwtUtil;
    @Autowired
    private HttpServletRequest httpServletRequest;
    @Autowired
    private MedicosRepository medicosRepository;

    public Atendimento salva(Atendimento atendimento) {

        return atendimentoRepository.save(atendimento);

    }

    public Atendimento adicionarMedico(Atendimento atendimento) {
        String emailDoMeDico = jwtUtil.getEmailDoUsuario(httpServletRequest.getHeader(HEADER_STRING));
        Medicos medico =  medicosRepository.findByEmail(emailDoMeDico);
        atendimento.setMedicos(medico);
        return atendimento;

    }

}
