package br.com.lucas.conexa.service;


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
 

import br.com.lucas.conexa.security.UserSS;
import br.com.lucas.conexa.models.Usuario;
import br.com.lucas.conexa.repository.UsuarioRepository;

@Transactional
@Service("userService")
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UsuarioRepository repo;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Usuario usuario = repo.findByEmail(email);
		if (usuario == null) {
			throw new UsernameNotFoundException("Usuario não encontrado! "+email);
		}
		return new UserSS(usuario.getId(),usuario.getEmail(),usuario.getSenha());
	}
	
	
	
	
	
}
