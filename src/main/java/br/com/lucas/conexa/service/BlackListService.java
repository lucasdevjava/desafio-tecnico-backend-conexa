package br.com.lucas.conexa.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lucas.conexa.models.BlackList;
import br.com.lucas.conexa.repository.BlackListResponsitory;

@Service
public class BlackListService {

    @Autowired
    private BlackListResponsitory repo;

    public BlackList salva(String token) {

        var blackList = new BlackList(null, token);
        repo.save(blackList);
        return blackList;
    }

    public BlackList findBlackList(String token) {
        var blackList = repo.findByToken(token);
        return blackList;
    }

}
