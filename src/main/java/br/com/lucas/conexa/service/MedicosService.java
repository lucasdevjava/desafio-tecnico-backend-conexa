package br.com.lucas.conexa.service;

import static java.lang.System.out;

import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.lucas.conexa.models.Medicos;
 
import br.com.lucas.conexa.models.enums.Perfil;
import br.com.lucas.conexa.repository.MedicosRepository;
import br.com.lucas.conexa.repository.RoleRepository;
import br.com.lucas.conexa.service.exception.DatabaseException;
import br.com.lucas.conexa.service.exception.ObjectNotFoundException;
import br.com.lucas.conexa.service.exception.UniqueFieldException;
import lombok.var;

@Service
public class MedicosService {


    private final BCryptPasswordEncoder encoder;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private MedicosRepository repo;

    public MedicosService(){
        this.encoder = new BCryptPasswordEncoder();
    }

    @Transactional
    public Medicos salvar(Medicos medicos) {

        this.checadoreEmailJaInformado(medicos.getEmail());
        try {
            this.criptografaSenha(medicos);
            var role = roleRepository.findBynomeRole(Perfil.MEDICO.name());

            medicos.addRole(role);
            role.addUsario(medicos);

            medicos = repo.save(medicos);

            return medicos;
        } catch (Exception e) {
            out.println(e.getMessage());
            throw new DatabaseException(e.getMessage());
        }

    }

    public void criptografaSenha(Medicos medicos) {
        var senhaCriptografada = encoder.encode(medicos.getSenha());
        medicos.setSenha(senhaCriptografada);
    }

    public void checadoreEmailJaInformado(String email) {
    var medico = repo.findByEmail(email);

        if (medico != null) {
            throw new UniqueFieldException("O e-mail já existe no sistema.");
        }
    }

    public Medicos buscarPorId(UUID id) {
        Optional<Medicos> medico =      repo.findById(id);
        
        return medico.orElseThrow(()->new ObjectNotFoundException("Medico não encontrado."));
    }

}
