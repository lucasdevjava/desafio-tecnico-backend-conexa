package br.com.lucas.conexa.response;

import static org.springframework.http.HttpStatus.CREATED;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.lucas.conexa.dtos.AtendimentoNovoDTO;
import br.com.lucas.conexa.models.Atendimento;
import br.com.lucas.conexa.service.AtendimentoService;

@RestController
@RequestMapping(value = "/attendance")
public class AtendimentoController {

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private AtendimentoService service;

    @PostMapping
    public ResponseEntity<AtendimentoNovoDTO> salvar(@Valid @RequestBody AtendimentoNovoDTO atendimentoNovoDTO) {

        var atendimento = modelMapper.map(atendimentoNovoDTO, Atendimento.class);
        service.adicionarMedico(atendimento);
        service.salva(atendimento);
        return ResponseEntity.status(CREATED).build();
    }

}

