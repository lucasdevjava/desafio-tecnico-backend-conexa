package br.com.lucas.conexa.response;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.lucas.conexa.service.BlackListService;

@RestController
@RequestMapping(value = "/logoff")
public class BlackListController {
    
    @Autowired
    private BlackListService blackListService;

    @Autowired
    private HttpServletRequest request;


    @PostMapping
    public ResponseEntity<Void> logoff() {
        String token = request.getHeader("Authorization");
        blackListService.salva(token);
 
        return ResponseEntity.ok().build();
    }

}
