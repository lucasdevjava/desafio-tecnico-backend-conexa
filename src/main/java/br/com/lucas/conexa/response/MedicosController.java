package br.com.lucas.conexa.response;

  
import static br.com.lucas.conexa.service.util.Util.comparadorDeSenha;
import static org.springframework.http.HttpStatus.CREATED;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
 
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

 
import br.com.lucas.conexa.dtos.MedicosNovo;
import br.com.lucas.conexa.models.Medicos;
import br.com.lucas.conexa.service.MedicosService;
 
@RestController
@RequestMapping(value = "/signup")
public class MedicosController {

    @Autowired
    private MedicosService service;
    @Autowired
    private ModelMapper modelMapper;
 

    @PostMapping
    public ResponseEntity<Void> salvar(@Valid @RequestBody MedicosNovo medicosNovo) {
        
        comparadorDeSenha(medicosNovo.getSenha(), medicosNovo.getConfirmacaoSenha());
        var medico = modelMapper.map(medicosNovo, Medicos.class);
        medico = service.salvar(medico);
        return ResponseEntity.status(CREATED).build();
    }
}