package br.com.lucas.conexa.models;


import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import org.springframework.security.core.GrantedAuthority;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
public class Role implements GrantedAuthority {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, unique = true, nullable = false)
    private UUID id;
    @Column( updatable = false, unique = true, nullable = false)
    private String nomeRole;

    @ManyToMany
    private List<Usuario> usuarios = new ArrayList<>();

    public Role(String nomeRole) {
        this.nomeRole = nomeRole;
    }

    @Override
    public String getAuthority() {

        return this.nomeRole;
    }

    public void addUsario(Usuario usuario) {
        this.usuarios.add(usuario);
    }

}

