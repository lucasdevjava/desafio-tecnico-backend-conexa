package br.com.lucas.conexa.models;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Atendimento implements Serializable {
    private static final long serialVersionUID = 1L;

   

    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, unique = true, nullable = false)
	private UUID id;

    
    private LocalDateTime dataHora;
    @Embedded
	@AttributeOverrides({ @AttributeOverride(name = "paciente", column = @Column(name = "PacienteAtendimento")) })
    private PacienteAtendimento paciente;

    @JsonIgnore
	@ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "medico_Id")
	private Medicos medicos;

}
