package br.com.lucas.conexa.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "TIPO", discriminatorType = DiscriminatorType.STRING)
public abstract class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, unique = true, nullable = false)
	private UUID id;
	@Column(unique = true)
	@Email(message = "O campo email não pode estar vazio.")
	@NotEmpty(message = "O campo email não pode estar vazio.")
	private String email;
	@NotEmpty(message = "O campo senha não pode estar vazio.")
	@JsonIgnore
	private String senha;

	@ManyToMany
	@JsonIgnore
	@JoinTable(name = "usario_roles", joinColumns = @JoinColumn(name = "usario_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
	private Set<Role> roles = new HashSet<>();

	public Usuario(UUID id, String email, String senha) {
		this.id = id;
		this.email = email;
		this.senha = senha;
	}

	@JsonIgnore
	public Set<Role> getRoles() {
		return roles;
	}

	@JsonIgnore
	public void setRoles(Set<Role> role) {
		this.roles = role;
	}

 

	public void addRole(Role role) {
		this.roles.add(role);
	}

}
