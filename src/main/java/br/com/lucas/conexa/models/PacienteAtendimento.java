package br.com.lucas.conexa.models;

import java.io.Serializable;

 
import javax.persistence.Embeddable;
 
import org.hibernate.validator.constraints.br.CPF;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class PacienteAtendimento implements Serializable {
	private static final long serialVersionUID = 1L;



  private String  nome;
  @CPF(message = "Informe um cpf valido") 
  private String cpf;

}
