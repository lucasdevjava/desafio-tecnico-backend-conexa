package br.com.lucas.conexa.models;

import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
public class Medicos extends Pessoa {
    private static final long serialVersionUID = 1L;

    private String especialidade;

    @OneToMany(mappedBy = "medicos", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Atendimento> atendimento;

    public Medicos(UUID id, String email, String senha, String cpf,
            String dataNascimento, String telefone, String especialidade) {
        super(id, email, senha, cpf, dataNascimento, telefone);
        this.especialidade = especialidade;
    }



}

