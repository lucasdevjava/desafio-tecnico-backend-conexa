package br.com.lucas.conexa.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.lucas.conexa.service.BDService;

@Configuration
public class InicializaçãoDoProjetoConfig {
    
   
    @Autowired
    private BDService bdService;

    @Bean
    public void inicilarBanco(){
     
        bdService.inicializarElementosNoBanco();
       
    }


}

