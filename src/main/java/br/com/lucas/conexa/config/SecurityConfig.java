package br.com.lucas.conexa.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;

 
 
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import br.com.lucas.conexa.security.JWTAuthenticationFilter;
import br.com.lucas.conexa.security.JWTAuthorizationFilter;
import br.com.lucas.conexa.security.JWTUtil;
import br.com.lucas.conexa.service.UserDetailsServiceImpl;

import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

        @Autowired
        UserDetailsServiceImpl userDetailsService;

        @Autowired
        JWTUtil jWTUtil;

        @Autowired
        private Environment env;

        @Autowired
        private br.com.lucas.conexa.repository.BlackListResponsitory blackListResponsitory;

        private static final String[] PUBLIC_MATCHERS = {
                        "/h2-console/**",
        };

        private static final String[] PUBLIC_MATCHERS_GET = {

                        "/motoclubes/**",
                        "/events/**",
                        "/users/**"
        };
        private static final String[] PUBLIC_MATCHERS_POST = {
                        "/api/v1/signup",
                        "/api/v1/login",
                        "/api/v1/attendance"
                         
        };

        @Override
        protected void configure(HttpSecurity http) throws Exception {

                if (Arrays.asList(env.getActiveProfiles()).contains("test")) {// ativa o banco do profile
                                                                              // conrrespondente.
                        http.headers().frameOptions().disable();
                }

                http.cors().and().csrf().disable();
                http.authorizeRequests()
                                .antMatchers(PUBLIC_MATCHERS).permitAll()
                                .antMatchers(HttpMethod.POST, PUBLIC_MATCHERS_POST).permitAll()
                                .antMatchers(HttpMethod.GET, PUBLIC_MATCHERS_GET).hasAnyRole("ADMIN", "MEDICO")
                                .antMatchers(HttpMethod.POST, "/admin/**").hasRole("ADMIN").and()
                                .addFilter(new JWTAuthenticationFilter(authenticationManager()))
                                .addFilter(new JWTAuthorizationFilter(authenticationManager(), jWTUtil,
                                                userDetailsService, blackListResponsitory));

                http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        }

        @Bean
        CorsConfigurationSource corsConfigurationSource() {
                CorsConfiguration configuration = new CorsConfiguration();
                configuration.setAllowedOrigins(Arrays.asList("*"));
                configuration.setAllowedHeaders(Arrays.asList("application/xml"));
                configuration.setAllowedHeaders(Arrays.asList("authorization", "Origin", "X-Requested-With", "Accept",
                                "Content-Type", "x-auth-token", "Access-Control-Allow-Origin"));
                configuration.setAllowedMethods(
                                Arrays.asList("POST", "GET", "PUT", "DELETE", "OPTIONS", "HEAD", "TRACE", "CONNECT"));
                UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
                source.registerCorsConfiguration("/**", configuration);
                return source;
        }

        @Override
        public void configure(AuthenticationManagerBuilder auth) throws Exception {
                auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
        }

        @Bean
        public BCryptPasswordEncoder bCryptPasswordEncoder() {
                return new BCryptPasswordEncoder();
        }

}
