package br.com.lucas.conexa.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CredicialDTO {
    
    private String email;
    private String senha;


}
