package br.com.lucas.conexa.dtos;

import java.time.LocalDateTime;
 

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.lucas.conexa.models.PacienteAtendimento;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AtendimentoNovoDTO {
  
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm:ss")
  private LocalDateTime dataHora;

   private PacienteAtendimento paciente;


}
