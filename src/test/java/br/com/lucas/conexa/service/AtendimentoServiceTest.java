package br.com.lucas.conexa.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.lucas.conexa.config.AplicationConfingTest;
import br.com.lucas.conexa.models.Atendimento;
import br.com.lucas.conexa.models.Medicos;
import br.com.lucas.conexa.models.PacienteAtendimento;
import br.com.lucas.conexa.models.Role;
import br.com.lucas.conexa.models.enums.Perfil;
import br.com.lucas.conexa.repository.AtendimentoRepository;
import br.com.lucas.conexa.repository.MedicosRepository;
import br.com.lucas.conexa.repository.RoleRepository;

@DisplayName(" AtendimentoService")
public class AtendimentoServiceTest extends AplicationConfingTest {

    @MockBean
    private AtendimentoRepository atendimentoRepository;

    @MockBean
    private RoleRepository roleRepository;

    @Autowired
    private AtendimentoService atendimentoService;

    @MockBean
    private MedicosRepository medicosRepository;
    @MockBean
    private BCryptPasswordEncoder encoder;

    @Test
    public void salvarTest() {
        var medico = medicosRepository.findByEmail("lucas@email.com");
        var dataHora = LocalDateTime.now();
        var pacienteAtendimento = new PacienteAtendimento("teste","124.8265.564-51");
        var atendimento = new Atendimento(null,dataHora,pacienteAtendimento,medico);

        atendimentoService.salva(atendimento);
 
       

    }

    @BeforeEach
    public void setup() {
        var medico = new Medicos(UUID.randomUUID(), "lucas@email.com", encoder.encode("123456"),
                "124.826.564-51",
                "24/11/1997",
                "(81)9 81232279",
                "TI");
        Mockito.when(medicosRepository.findByEmail(medico.getEmail())).thenReturn(medico);

        var role = new Role(Perfil.MEDICO.name());
        Mockito.when(roleRepository.findBynomeRole(Perfil.MEDICO.name())).thenReturn(role);

    }
}
