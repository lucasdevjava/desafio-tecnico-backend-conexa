package br.com.lucas.conexa.response;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;

import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import br.com.lucas.conexa.config.AplicationConfingTest;
import br.com.lucas.conexa.dtos.MedicosNovo;
import br.com.lucas.conexa.models.Role;
import br.com.lucas.conexa.models.enums.Perfil;
import br.com.lucas.conexa.repository.MedicosRepository;
import br.com.lucas.conexa.repository.RoleRepository;
import br.com.lucas.conexa.service.MedicosService;
 

@DisplayName("MedicosController")
public class MedicosControllerTest extends AplicationConfingTest {
    @MockBean
    private BCryptPasswordEncoder encoder;
    @MockBean
    private MedicosService medicosService;
    @MockBean
    private MedicosRepository repo;

    @MockBean
    private RoleRepository roleRepository;

    @Autowired
    private MockMvc mockMvc;

 

    @Test
    public void testSalva() throws JsonProcessingException, Exception {

        var medicosNovo = new MedicosNovo("lucas@gmail.com", "123456", "123456", "124.826.564-51", "24/11/1997",
                "(81)981232279", "TI");
               var gson = new Gson();
                
        mockMvc.perform(post("/api/v1/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(medicosNovo)))
                .andExpect(MockMvcResultMatchers.status().isCreated());

    }


    @BeforeEach
    public void setup() {
        

        var role = new Role(Perfil.MEDICO.name());
        Mockito.when(roleRepository.findBynomeRole(Perfil.MEDICO.name())).thenReturn(role);

    }

}
