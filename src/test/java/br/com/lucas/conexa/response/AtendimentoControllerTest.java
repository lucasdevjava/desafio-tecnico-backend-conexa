package br.com.lucas.conexa.response;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

 
import br.com.lucas.conexa.security.JWTUtil;
import br.com.lucas.conexa.config.AplicationConfingTest;
import br.com.lucas.conexa.dtos.AtendimentoNovoDTO;
import br.com.lucas.conexa.models.Medicos;
import br.com.lucas.conexa.models.PacienteAtendimento;
import br.com.lucas.conexa.repository.AtendimentoRepository;
import br.com.lucas.conexa.repository.BlackListResponsitory;
import br.com.lucas.conexa.repository.MedicosRepository;
 
import br.com.lucas.conexa.service.AtendimentoService;
import br.com.lucas.conexa.service.MedicosService;

@DisplayName("AtendimentoController")
public class AtendimentoControllerTest extends AplicationConfingTest {
    

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BCryptPasswordEncoder encoder;

    @MockBean
    private AtendimentoService atendimentoService;

    @MockBean
    private BlackListResponsitory blackListResponsitory;

    @MockBean
    private AtendimentoRepository atendimentoRepository;
    @MockBean
    private JWTUtil jwtUtil;
    @MockBean
    private HttpServletRequest httpServletRequest;
    @MockBean
    private MedicosService medicosService;

    @MockBean
    private MedicosRepository medicosRepository;

  

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testSalva() throws JsonProcessingException, Exception {
        var dataHora = LocalDateTime.now();
        var pacienteAtendimento = new PacienteAtendimento("teste","124.8265.564-51");
        var atendimento = new AtendimentoNovoDTO( dataHora, pacienteAtendimento);

       

        mockMvc.perform(post("/api/v1/attendance")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(atendimento))
                .header("Authorization","Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJsdWNhc0BlbWFpbC5jb20iLCJpZCI6ImU4YWNlNWNhLWRkZTQtNDUwNS1iNjE2LWY4MmQ4MWVjMzQ5NSIsImVtYWlsIjoibHVjYXNAZW1haWwuY29tIiwiZXhwIjoxNjU5NjM0MjA0fQ.wHipZf3-7PU-tCPdrzfe8U8WMzr0wAx7Kogky46w9G5mpwnD7qWRS8eHqylErp86oJ4IBgxhAC_t1y2F_lVUOA")
                )
                .andExpect(MockMvcResultMatchers.status().isCreated());

    }


    @BeforeEach
    public void setup() {
            var medico = new Medicos(UUID.fromString("e8ace5ca-dde4-4505-b616-f82d81ec3495"), "lucas@email.com", encoder.encode("123456"),
                            "124.826.564-51",
                            "24/11/1997",
                            "(81)9 81232279",
                            "TI");
            Mockito.when(medicosRepository.findByEmail( "lucas@email.com")).thenReturn(medico);
    }

}
